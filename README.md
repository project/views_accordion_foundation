CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Views Accordion Foundation module provides a display style plugin for the Views
module. It will take the results and display them as a ZURB Foundation
accordion, using the first field as the header for the accordion rows.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/views_accordion_foundation

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/views_accordion_foundation

SPECIAL THANKS TO Manuel Garcia - https://www.drupal.org/u/manuel-garcia who
created the views_accordion (https://www.drupal.org/project/views_accordion)
module which this module was inspired by.


REQUIREMENTS
------------

 * This module requires NO modules outside of Drupal core.
 * This module requires ZURB Foundation Core + Accordion to be available.
 * The views_accordion module, which this module is based on is also NOT
  required, because this module was created separately due to too less
  similarities.


INSTALLATION
------------

 * Install the Views Accordion Foundation module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Views and create or edit a view.
    3. Choose Accordion (ZURB Foundation) in the Style dialog within your view,
       which will prompt you to configure the display settings.

Your view must meet the following requirements:
 * Row style must be set to Fields.
 * Provide at least two fields to show.

Please note:
The first field WILL be used as the header for each accordion section, all
others will be displayed when the header is clicked. The module creates an
accordion section per row of results from the view.


MAINTAINERS
-----------

 * Julian Pustkuchen (Anybody) - https://www.drupal.org/u/anybody

Supporting organization:
 * DROWL.de - https://www.drupal.org/drowlde
