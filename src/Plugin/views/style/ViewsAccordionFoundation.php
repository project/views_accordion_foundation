<?php

namespace Drupal\views_accordion_foundation\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Style plugin to render each item in an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "views_accordion_foundation",
 *   title = @Translation("Accordion (ZURB Foundation)"),
 *   help = @Translation("Display a ZURB Foundation Accordion with the results. The first field will be used as the header and trigger."),
 *   theme = "views_accordion_foundation_view",
 *   display_types = {"normal"}
 * )
 */
class ViewsAccordionFoundation extends StylePluginBase {
  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = TRUE;

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['row-start-open'] = ['default' => 1];
    $options['multi-expand'] = ['default' => 0];
    $options['allow-all-closed'] = ['default' => 0];
    $options['disabled'] = ['default' => 0];
    $options['tabs-small'] = ['default' => 1];
    $options['tabs-medium'] = ['default' => 0];
    $options['tabs-large'] = ['default' => 0];
    $options['slide-speed'] = ['default' => 250];
    $options['deep-link'] = ['default' => 1];
    $options['deep-link-smudge'] = ['default' => 0];
    $options['deep-link-smudge-delay'] = ['default' => 300];
    $options['deep-link-update-history'] = ['default' => 0];
    $options['class'] = ['default' => ''];
    $options['wrapper_class'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Find out how many items the display is currently configured to show
    // (row-start-open).
    $maxitems = $this->displayHandler->getOption('items_per_page');
    // If items_per_page is set to unlimitted (0), 10 rows will be what the user
    // gets to choose from.
    $maxitems = ($maxitems == 0) ? 10 : $maxitems;

    // Setup our array of options for choosing which row should start opened
    // (row-start-open).
    $rsopen_options = [];
    for ($i = 1; $i <= $maxitems; $i++) {
      $rsopen_options[$i] = $this->t('Row @number', ['@number' => $i]);
    }
    $rsopen_options['last'] = $this->t('Last');
    $rsopen_options['none'] = $this->t('None');

    $form['row-start-open'] = [
      '#type' => 'select',
      '#title' => $this->t('Row to display opened on start'),
      '#default_value' => $this->options['row-start-open'],
      '#description' => $this->t('Choose which row should start opened when the accordion first loads. If you want all to start closed, choose "None", and make sure to have "Allow for all rows to be closed" on below. Default: 1'),
      '#options' => $rsopen_options,
    ];
    $form['multi-expand'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Multi-expand'),
      '#default_value' => $this->options['multi-expand'],
      '#description' => $this->t('By default, only one pane of an accordion can be open at a time. This can be changed by setting this (multiExpand) option to true. Default: false'),
    ];
    $form['allow-all-closed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow all closed'),
      '#default_value' => $this->options['allow-all-closed'],
      '#description' => $this->t('By default, at least one pane in an accordion must be open. This can be changed by setting this (allowAllClosed) option to true. Default: false'),
    ];
    $form['tabs-small'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display as Tabs on small devices'),
      '#default_value' => $this->options['tabs-small'],
      '#description' => $this->t('Display as tabs on small devices (Responsive Accordion Tabs). Default: true'),
    ];
    $form['tabs-medium'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display as Tabs on medium devices'),
      '#default_value' => $this->options['tabs-medium'],
      '#description' => $this->t('Display as tabs on medium devices (Responsive Accordion Tabs). Default: false'),
    ];
    $form['tabs-large'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display as Tabs on large devices'),
      '#default_value' => $this->options['tabs-large'],
      '#description' => $this->t('Display as tabs on large devices (Responsive Accordion Tabs). Default: false'),
    ];
    $form['slide-speed'] = [
      '#type' => 'number',
      '#title' => $this->t('Slide speed (ms)'),
      '#step' => 1,
      '#min' => 0,
      '#size' => 5,
      '#default_value' => $this->options['slide-speed'],
      '#description' => $this->t('Amount of time (ms) to animate the opening of an accordion pane. Default: 250'),
    ];
    $form['deep-link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Deep link (state via URL Hash)'),
      '#default_value' => $this->options['deep-link'],
      '#description' => $this->t('When the data-deep-link option is set to true, the current state of the accordion is recorded by adding a hash with the accordion panel ID to the browser URL when a accordion opens. Default: false'),
    ];
    $form['deep-link-smudge'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Deep link > Smudge'),
      '#default_value' => $this->options['deep-link-smudge'],
      '#description' => $this->t('If deep-link is enabled, adjust the deep link scroll to make sure the top of the accordion panel is visible. Default: false'),
      '#states' => [
        'visible' => [
          ':input[name="style_options[deep-link]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['deep-link-smudge-delay'] = [
      '#type' => 'number',
      '#title' => $this->t('Deep link > Smudge Delay (ms)'),
      '#step' => 1,
      '#min' => 0,
      '#size' => 5,
      '#default_value' => $this->options['deep-link-smudge-delay'],
      '#description' => $this->t('If deep-link-smudge is enabled, animation time (ms) for the deep link adjustment. Default: 300'),
      '#states' => [
        'visible' => [
          ':input[name="style_options[deep-link-smudge]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['deep-link-update-history'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Deep link > Update History'),
      '#default_value' => $this->options['deep-link-update-history'],
      '#description' => $this->t('By default, accordion replace the browser history (using history.replaceState()). Modify this behavior by using settings this option to true to append to the browser history. Default: false'),
      '#states' => [
        'visible' => [
          ':input[name="style_options[deep-link]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['disabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable pane switching'),
      '#default_value' => $this->options['disabled'],
      '#description' => $this->t('There may be times where you want to disable pane switching on an accordion. This can be accomplished by setting this (disabled) option to true. Default: false'),
    ];
    $form['wrapper_class'] = [
      '#title' => $this->t('Additional wrapper class'),
      '#description' => $this->t('The additional class(es) to provide on the wrapper, outside the list.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['wrapper_class'],
    ];
    $form['class'] = [
      '#title' => $this->t('Additional list class'),
      '#description' => $this->t('The additional class(es) to provide on the list element itself.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['class'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($result) {
    // Provide settings from options:
    $view_settings = [
      'view_id' => $this->view->dom_id,
      'row_start_open' => $this->options['row-start-open'],
      'multi_expand' => $this->options['multi-expand'],
      'allow_all_closed' => $this->options['allow-all-closed'],
      'disabled' => $this->options['disabled'],
      'tabs_small' => $this->options['tabs-small'],
      'tabs_medium' => $this->options['tabs-medium'],
      'tabs_large' => $this->options['tabs-large'],
      'slide-speed' => $this->options['slide-speed'],
      'deep_link' => $this->options['deep-link'],
      'deep_link_smudge' => $this->options['deep-link-smudge'],
      'deep_link_smudge_delay' => $this->options['deep-link-smudge-delay'],
      'deep_link_update_history' => $this->options['deep-link-update-history'],
    ];

    // Prepare the JS settings.
    // We do it here so we don't have it run once every group.
    if ($this->options['row-start-open'] == 'last') {
      $view_settings['row_start_open'] = 'last';
    }
    else {
      $view_settings['row_start_open'] = ($this->options['row-start-open'] == 'none') ? 'none' : (int) $this->options['row-start-open'];
    }

    // The view display selector.
    // Set in stable & classy themes.
    $view_settings['display'] = '.js-view-dom-id-' . $this->view->dom_id;

    // Provide settings in JS:
    $this->view->element['#attached']['drupalSettings']['views_accordion_foundation'] = [$this->view->dom_id => $view_settings];
    // Attach libraries:
    if ($view_settings['tabs_small'] || $view_settings['tabs_medium'] || $view_settings['tabs_large']) {
      // If one tab option is enabled, we need responsiveAccordionTabs:
      $this->view->element['#attached']['library'][] = 'foundation_sites/responsiveAccordionTabs';
    }
    else {
      // Otherwise the simple lib is enough:
      $this->view->element['#attached']['library'][] = 'foundation_sites/accordion';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $errors = parent::validate();
    if (!$this->usesFields()) {
      $errors[] = $this->t('Select Format > Display: Fields. Views accordion foundation requires "Fields" to be selected as row style');
    }

    if ($this->options['allow-all-closed'] !== 1 && $this->options['row-start-open'] === 'none') {
      $errors[] = $this->t('Setting "Row to display opened on start" to "None" requires "allow-all-closed" to be enabled.');
    }
    return $errors;
  }

}
