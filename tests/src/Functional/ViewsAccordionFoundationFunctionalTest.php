<?php

namespace Drupal\Tests\views_accordion_foundation\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for the Views Accordion Foundation module.
 *
 * @group views_accordion_foundation
 */
class ViewsAccordionFoundationFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'views',
    'foundation_sites',
    'views_accordion_foundation',
    'views_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();

    $user = $this->drupalCreateUser([
      'access administration pages',
      'administer views',
    ]);
    $this->drupalLogin($user);
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    $this->drupalLogout();
    $this->drupalLogin($this->adminUser);
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-views-accordion-foundation');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm deinstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');
  }

  /**
   * Tests creating a view with the page style plugin.
   *
   * Tests creating a view with the views_accordion_foundation page style plugin
   * and checking, that alle the default values are set.
   */
  public function testViewsAccord() {
    $session = $this->assertSession();
    // Test views add form.
    $edit = [
      'id' => 'test',
      'label' => 'Test',
      'show[wizard_key]' => 'node',
      'show[sort]' => 'none',
      'page[create]' => 1,
      'page[title]' => 'Test',
      'page[path]' => 'test',
      'page[style][style_plugin]' => 'views_accordion_foundation',
      'page[style][row_plugin]' => 'teasers',
    ];
    $this->drupalGet('admin/structure/views/add');
    $session->statusCodeEquals(200);
    $this->submitForm($edit, 'Save and edit');
    $session->pageTextContains('Select Format > Display: Fields. Views accordion foundation requires "Fields" to be selected as row style');
    $edit['page[style][row_plugin]'] = 'fields';
    $this->drupalGet('admin/structure/views/add');
    $session->statusCodeEquals(200);
    $this->submitForm($edit, 'Save and edit');
    $session->pageTextContains('The view test has been saved.');

    // Assert the options of our exported view display correctly.
    $this->drupalGet('admin/structure/views/view/test/edit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Test (Content)');

    // Verify the style options show with the default values in the form:
    $this->drupalGet('admin/structure/views/nojs/display/test/page_1/style_options');
    $session->statusCodeEquals(200);
    $session->fieldValueEquals('style_options[row-start-open]', 1);
    $session->checkboxNotChecked('style_options[multi-expand]');
    $session->checkboxNotChecked('style_options[allow-all-closed]');
    $session->checkboxNotChecked('style_options[disabled]');
    $session->checkboxChecked('style_options[tabs-small]');
    $session->checkboxNotChecked('style_options[tabs-medium]');
    $session->checkboxNotChecked('style_options[tabs-large]');
    $session->fieldValueEquals('style_options[slide-speed]', '250');
    $session->checkboxChecked('style_options[deep-link]');
    $session->checkboxNotChecked('style_options[deep-link-smudge]');
    $session->fieldValueEquals('style_options[deep-link-smudge-delay]', '300');
    $session->checkboxNotChecked('style_options[deep-link-update-history]');
    $session->fieldValueEquals('style_options[class]', '');
    $session->fieldValueEquals('style_options[wrapper_class]', '');
  }

}
